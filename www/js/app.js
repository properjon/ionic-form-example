// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngMessages'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.controller('ContactCtrl', function($scope, $state, $ionicModal) {
  $scope.contact = {};
  $scope.contactForm = {};

  $ionicModal.fromTemplate('form-success-modal', {
    scope: $scope,
    animation: 'slide-in-up'
  });

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

  $scope.submit = function(form) {
    if(form.$valid) {
      $scope.modal.show();
    }
  };
})

.directive('validUsPhoneNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$validators.validUsPhoneNumber = function(modelValue) {
        modelValue = (typeof modelValue == "number") ? modelValue.toString() : modelValue;

        return isValidNumber(modelValue, "US");
      }
    }
  }
})
.directive('safeString', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      function checkSocialSecurity(input) {
        var matches = input.match(/\d{9}/g);

        return (matches != null) ? true : false;
      }

      function checkCreditCard(input) {
        var matches = input.match(/\d{16}/g);

        return (matches != null) ? true : false;
      }

      ngModel.$validators.safeString = function(modelValue) {
        if (modelValue === undefined) return true;
        modelValue = modelValue.replace(/-|\s+/g, "");

        return (checkCreditCard(modelValue) || checkSocialSecurity(modelValue)) ? false : true;
      }
    }
  }
});